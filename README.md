# Machine Learning for Tax Administrations Workshop

A World Bank five-day induction workshop to Data Sciences and Machine Learning, designed as a practical introduction to the concepts and tools used in Machine Learning and Data Sciences.  

By the end of the Workshop, participants should be familiar with the basic terminology and should have sufficient resources to start experimenting with Machine Learning and Data Sciences.  

Each induction session will be two hours in length, where the moderators will work through the concepts and tools that are used to solve problems using Visualization, Predictive Analysis, Clustering, Classification and Machine Learning, to identify trends, patterns, and classes of behaviors from vast quantities of data with a high number of variables.

The workshop will be technical and requires that the participants have basic programming experience in a high-level programming language, such as Java, C# or Python.  The workshop will use the Python programming langue and will use Jupyter notebooks, since these allow interactive exploration and development of Machine Learning Solutions. 

All the material presented will be open-source or will be shared with the participants, who can use it as a starting point for future development of Machine Learning projects. 

The workshop is an introduction and will cover the foundations of Machine Learning with emphasis on basic principles and practice, specifically oriented towards linear regression, classification, and clustering, with examples that will be similar to typical applications in a Tax Administration.  

Given the level of complexity of Machine Learning, this workshop will not go into much depth into the mathematical theory behind each of the techniques presented but will present the overall concepts so that participants can familiarize themselves with the techniques, the terminology and get some applied experience. 
