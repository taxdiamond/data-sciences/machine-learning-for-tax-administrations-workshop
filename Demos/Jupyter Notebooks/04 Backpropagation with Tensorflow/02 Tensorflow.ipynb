{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 02 - Calculating Gradients with Tensorflow "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import tensorflow as tf\n",
    "import numpy as np\n",
    "import tensorflow.keras.optimizers as optimizer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Suppose we have a table of known values \n",
    "\n",
    "|X|Y| \n",
    "|---|---|\n",
    "|1|3|\n",
    "|2|4|\n",
    "|3|7|\n",
    "|4|9|\n",
    "|5|12|\n",
    "\n",
    "Our objective is to \"teach\" the Machine Learning algorithm to predict what would be the value of $Y$ when we feed it some new value $X$.  For example, what would be the value of $Y$ when $X = 15$\n",
    "\n",
    "When we are training a Machine Learning algorithm, our ML machine implements some function $f(x,w)$, where $x$ is our observation and $w$ is the Machine Learning parameter we will \"train\".  Our strategy is simple:\n",
    "\n",
    "1.- Guess the initial value of the parameter.\n",
    "\n",
    "2.- Calculate $z = f(x,w)$ with the first observation $x$ and then measure the difference between $z$ and the the known label $y$.  We will call this our Loss function or $L(x,y)$.  Our Machine Learning algorithm will be **perfect** when the Loss function is **zero**.  This means that there are no errors between our prdiction and the observed value Y.\n",
    "\n",
    "Suppose that our Loss Function is $(w-5)^2$.  We would need to find the value of $w$ that minimizes the loss function, or: $\\textrm{min}(w-5)^2$.  \n",
    "\n",
    "We will try to use Tensorflow to find the value that minimizes the function. \n",
    "\n",
    "Normally, we would need to calculate the Gradient of the Loss Function with respect to the parameter.  This would imply calculating the partial derivative of the Loss Function with respect to the parameter and then adjusting the parameter a little in the direction of the gradient.  This gets to be very complicated when you are dealing with learning functions.\n",
    "\n",
    "This is where Tensorflow does its magic.  tf.GradientTape allows us to track TensorFlow computations and calculate gradients with respect to some given variables.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Guess a value of -3 for the parameter $w$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "w = tf.Variable(-3, dtype=tf.float32)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Optimizers are methods used to change the attributes of your machine/deep learning model such as weights and learning rate in order to reduce the losses. Optimizers help to get results faster.  The Adam optimization is a stochastic gradient descent method that is based on adaptive estimation of first-order and second-order moments.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "optimizer = tf.keras.optimizers.Adam(0.1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Define an algorithm to take a single step in the direction of the minimum value for the Loss Function.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "def traning_step():\n",
    "    with tf.GradientTape() as tape:\n",
    "        loss = (w - 5)**2\n",
    "    trainable_variables = [w]\n",
    "    grads = tape.gradient(loss,  trainable_variables)\n",
    "    optimizer.apply_gradients(zip(grads, trainable_variables))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Start with the initial guess of -3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<tf.Variable 'Variable:0' shape=() dtype=float32, numpy=-3.0>\n"
     ]
    }
   ],
   "source": [
    "print(w)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Take a single step.  The algorithm will automatically \"inch\" us in the general direction of the minimum optimum value of the loss function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "traning_step()\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<tf.Variable 'Variable:0' shape=() dtype=float32, numpy=-2.9>\n"
     ]
    }
   ],
   "source": [
    "print(w)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The value went from -3 to -2.9.  A step in the right direction.  Lets take a new step."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "traning_step()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<tf.Variable 'Variable:0' shape=() dtype=float32, numpy=-2.8000352>\n"
     ]
    }
   ],
   "source": [
    "print(w)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The value went from -2.9 to -2.8.  A second step in the right direction."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each step we take is in the correct direction, since Tensorflow is computing the Gradient (the direction) automatically.\n",
    "\n",
    "<img src=\"GradientDescent.png\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets take 1000 more steps."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<tf.Variable 'Variable:0' shape=() dtype=float32, numpy=4.9999976>\n"
     ]
    }
   ],
   "source": [
    "for i in range(1000):\n",
    "    traning_step()\n",
    "print(w)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The value went to 4.99999976.  This is basically 5, which is the correct value.  \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.6"
  },
  "metadata": {
   "interpreter": {
    "hash": "57baa5815c940fdaff4d14510622de9616cae602444507ba5d0b6727c008cbd6"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
